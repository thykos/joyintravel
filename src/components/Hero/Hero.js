import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.scss';
import classnames from 'classnames';

const colors = {
  '#FEE900': 'hero-sun',
  '#fea203': 'hero-orange',
  '#ff0063': 'hero-playboy',
  '#03cbff': 'hero-marine',
  '#4dce00': 'hero-forest',
  '#003d92': 'hero-sea',
  '#d80183': 'hero-pink'
};

export default class Hero extends Component {
  static propTypes = {
    title: PropTypes.string,
    fields: PropTypes.array.isRequired,
    color: PropTypes.string,
    mainFields: PropTypes.array.isRequired
  };

  render() {
    const { title, fields, mainFields, color } = this.props;
    return (
      <div className={classnames('heroContainer', colors[color])}>
        <div className='heroWrapper'>
          <div className='heroHeader'>
            {mainFields.map((field, idx) => <div key={idx}>{field}</div>)}
          </div>
          <div className='heroBody'>
            {title && <div className='heroTitle'>{title}</div>}
            {fields.map((field, idx) => <div key={idx}>{field}</div>)}
          </div>
        </div>
      </div>
    );
  }
}
