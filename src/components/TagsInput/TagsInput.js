import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TagsInput from 'react-tagsinput';
import './styles.scss';

export default class Tags extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    value: PropTypes.array
  };

  renderTag = ({ tag, key }) => {
    return (
      <div key={key} className='rti-tag'>
        <span>{tag}</span>
      </div>
    );
  };

  render() {
    const { onChange = () => {}, value = [], ...other} = this.props;
    return (
      <TagsInput value={value === '' ? [] : value}
                 className='form-control rti-wrapper'
                 onChange={onChange}
                 renderTag={props => this.renderTag(props)}
                 {...other}/>
    );
  }
}
