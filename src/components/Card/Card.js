import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './styles.scss';

const colors = {
  '#FEE900': 'card-sun',
  '#fea203': 'card-orange',
  '#ff0063': 'card-playboy',
  '#03cbff': 'card-marine',
  '#4dce00': 'card-forest',
  '#003d92': 'card-sea',
  '#d80183': 'card-pink'
};

export default class Card extends Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    color: PropTypes.string
  };

  render() {
    const { children, color, className } = this.props;
    return (
      <div className={classnames('cardContainer', colors[color], className)}>
        <div className='cardWrapper'>
          {children}
        </div>
      </div>
    );
  }
}
