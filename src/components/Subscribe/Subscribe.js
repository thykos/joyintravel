import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';
import { Button } from 'react-bootstrap';
import { reduxForm, Field, reset, SubmissionError } from 'redux-form';
import errorsHandler from '../../helpers/errorsHandler';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import './styles.scss';
import { renderInput, renderMaskedInput } from '../../helpers/formHelpers';

class Subscribe extends Component {
  static propTypes = {
    color: PropTypes.string,
    reset: PropTypes.func.isRequired
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };

  static Form = reduxForm({
    form: 'subscribeForm',
    fields: ['first_name', 'last_name', 'phone', 'email', 'student_card', 'university', 'social']
  })(({handleSubmit}) =>
    <form onSubmit={handleSubmit} className="subscribeForm">
      <div className="subscribeInputs">
        <Field component={renderInput}
               placeholder="Имя"
               required
               className="subscribeInput"
               name="first_name"
               />
        <Field component={renderInput}
               placeholder="Фамилия"
               required
               className="subscribeInput"
               name="last_name"
               />
        <Field component={renderMaskedInput}
               placeholder="+38(095)-123-45-67"
               name="phone"
               required
               className="subscribeInput"
               mask="+38(011)-111-11-11"
               />
        <Field component={renderInput}
               placeholder="Email"
               type="email"
               required
               className="subscribeInput"
               name="email"
               />
        <Field component={renderInput}
               placeholder="Студенческий(если есть)"
               type="text"
               className="subscribeInput"
               name="student_card"
               />
        <Field component={renderInput}
               placeholder="Учебное заведение"
               type="text"
               className="subscribeInput"
               name="university"
               />
        <Field component={renderInput}
               placeholder="Страница в социальной сети(Facebook или Vk)"
               type="text"
               className="subscribeInput"
               name="social"
               />
      </div>
      <div className="subscribeBtnWrapper">
        <Button bsSize="large" type="submit" className="subscribeBtn">Отправить</Button>
      </div>
    </form>
  );

  onSubmit = (formData) => {
    const { tourId } = this.props;
    return this.context.client.post('/tourists', {data: {resource: { ...formData, tour_id: tourId }}})
      .then(() => {
        this.props.reset('subscribeForm');
        toastr.success('Вы успешно зарегистрировались на тур');
      })
      .catch(response => {
        throw new SubmissionError(errorsHandler(response));
      });
  };

  render() {
    const { color } = this.props;
    return(
      <Card color={color} className="subscribeAngle">
        <div className="subscribeWrapper">
          <div className="subscribeHeader">Зарегистрироваться на тур</div>
          <div className="subscribeBody">
            <Subscribe.Form onSubmit={this.onSubmit}/>
          </div>
        </div>
      </Card>
    )
  }
}

export default connect(null, { reset })(Subscribe);