import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './styles.scss';
import Mia from '../../static/images/Mia.jpg';
import Miab from '../../static/images/Miab.jpg';
import Mic from '../../static/images/Mic.jpg';
import Mip from '../../static/images/Mip.jpg';
import Miph from '../../static/images/Miph.jpg';
import Miv from '../../static/images/Miv.jpg';
import logoDark from '../../static/images/logo_dark.png';
import logoWhite from '../../static/images/logo_white.png';

const menuItems = [
  {label: 'О нас', image: Miab, className: 'ab', link: '/'},
  {label: 'Туры', image: Mia, className: 'a', link: '/tours'},
  {label: 'Контакты', image: Mic, className: 'c', link: '/'},
  {label: 'Отзывы', image: Mip, className: 'p', link: '/'},
  {label: 'Фотоотчет', image: Miph, className: 'ph', link: '/'},
  {label: 'Видеоотчет', image: Miv, className: 'v', link: '/'}
];

export default class Menu extends Component {
  static propTypes = {
    push: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
  }

  handleShow = () => {
    this.setState({show: !this.state.show});
  };

  goToPage = (link) => {
    this.setState({
      show: false
    });
    this.props.push(link);
  };

  render() {
    const { show } = this.state;
    return (
      <div className='menuContainer'>
        {show &&
          <div className='menu'>
            <div className='wrapper'>
              {menuItems.map((item, idx) =>
                <div key={idx} onClick={() => this.goToPage(item.link)} className='menuItemWrapper'>
                  <div className={classnames('menuItem', item.className)}>
                    <img src={item.image} draggable={false} alt={item.label}/>
                    <div className='menuItemName'>{item.label}</div>
                  </div>
                </div>
              )}
            </div>
          </div>
        }
        <div className={classnames('menuWrapper', show && 'showWrapper')}>
          <div onClick={() => this.handleShow()}
               className={classnames('hamburger', 'fa',
                 { 'fa-bars': !show, 'fa-close': show})}/>
          <div className='contacts'>
            <div>+38(066) 362 03 42</div>
            <div>+38(050) 135 82 62</div>
          </div>
        </div>
        <div className='logo' onClick={() => this.goToPage('/')}>
          <img src={show ? logoDark : logoWhite} alt="Joy in Travel"/>
        </div>
      </div>
    );
  }
}
