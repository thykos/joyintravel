import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.scss';
import Card from '../Card/Card';
import moment from 'moment';

export default class Day extends Component {
  static propTypes = {
    day: PropTypes.object.isRequired,
    color: PropTypes.string
  };

  render() {
    const { day, color } = this.props;
    return (
      <Card color={color} className='day-angle'>
        <div className='day-wrapper'>
          <div className='day-imgWrapper'>
            <img src={`${day.image.large.url}`} alt={day.day}/>
          </div>
          <div className='day-textWrapper'>
            <div className='day-title'>
              <span>День {parseInt(day.day, 10) + 1}<small style={{color: color}}>{moment(day.date).format('DD-MM-YYYY')}</small></span>
            </div>
            <div className='day-description'>
              {day.description}
            </div>
          </div>
        </div>
      </Card>
    );
  }
}
