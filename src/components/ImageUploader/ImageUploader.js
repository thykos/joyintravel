import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import tempImageRequest from '../../helpers/tempImageRequest';
import './styles.scss';
import { toastr } from 'react-redux-toastr';

export default class ImageUploader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tempImage: null
    };
  }

  static propTypes = {
    onTempImage: PropTypes.func.isRequired,
    value: PropTypes.any
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };

  onDrop(files) {
    files.forEach(file => {
      tempImageRequest(this.context.client, file)
        .then(tempImage => {
          this.props.onTempImage(tempImage.image);
          this.setState({ tempImage });
        })
        .catch(response => toastr.error(response.errors.image.join(';\n')));
    });
  }

  removeTempImage() {
    this.props.onTempImage(null);
    this.setState({ tempImage: null });
  }

  render() {
    const { value } = this.props;
    const { tempImage } = this.state;
    return (
      <div>
        {!tempImage && !value &&
        <Dropzone ref="dropzone" onDrop={(files) => this.onDrop(files)} multiple={false}
                  className='iu-dropzone' activeClassName='iu-activeDropzone'>
            <div>Перетяните в эту зону изображение, либо нажмите и укажите путь.</div>
          </Dropzone>
        }
        <div className='iu-imagesPreviews'>
          {tempImage ?
            <div className='iu-imagePreview'>
              <img src={tempImage.image.thumb.url} alt="temp"/>
              <span className='iu-imagePreviewRemove' onClick={() => this.removeTempImage()}><span className="fa fa-minus"/></span>
            </div> :
            value && <div className='iu-imagePreview'>
              <img src={value.thumb.url} alt="temp"/>
            </div>
          }
        </div>
      </div>
    );
  }
}
