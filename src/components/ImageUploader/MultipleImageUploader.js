import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import without from 'lodash/without';
import tempImageRequest from '../../helpers/tempImageRequest';
import './styles.scss';
import { toastr } from 'react-redux-toastr';

export default class MultiImagesField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tempImages: []
    };
  }
  static propTypes = {
    onRemove: PropTypes.func,
    onTempImages: PropTypes.func.isRequired,
    value: PropTypes.array,
    removingImages: PropTypes.array,
    tempImagesIds: PropTypes.array
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };

  static defaultProps = {
    removingImages: []
  };

  componentWillReceiveProps = (nextProps) => {
    if (!nextProps.tempImagesIds || !nextProps.tempImagesIds.length) this.setState({tempImages: []});
  };

  onDrop(files) {
    files.forEach(file => {
      tempImageRequest(this.context.client, file)
        .then(tempImage => {
          const tempImages = [...this.state.tempImages, tempImage];
          this.props.onTempImages(tempImages.map(item => item.id));
          this.setState({ tempImages });
        })
        .catch(response => toastr.error(response.errors.image.join(';\n')));
    });
  }

  removeTempImage(tempImage) {
    return () => {
      const tempImages = without(this.state.tempImages, tempImage);
      this.props.onTempImages(tempImages.map(item => item.id));
      this.setState({ tempImages });
    };
  }

  removeImage(index) {
    const { onRemove, removingImages } = this.props;
    return onRemove(index, [...removingImages, index]);
  }

  render() {
    const { value, removingImages } = this.props;
    const { tempImages } = this.state;
    return (
      <div>
        <Dropzone ref="dropzone" onDrop={(files) => this.onDrop(files)} className='iu-dropzone' activeClassName='iu-activeDropzone'>
          <div>Перетяните в эту зону изображения, либо нажмите и укажите путь.</div>
        </Dropzone>
        <div className='iu-imagesPreviews'>
          {tempImages.map(tempImage =>
            <div className='iu-imagePreview' key={tempImage.id}>
              <img src={tempImage.image.thumb.url} alt="temp"/>
              <span className='iu-imagePreviewRemove' onClick={this.removeTempImage(tempImage)}><span className="fa fa-minus"/></span>
            </div>
          )}
          {value && value.map((image, index) =>
            removingImages.indexOf(index) === -1 &&
            <div className='iu-imagePreview' key={index}>
              <img src={image.thumb.url} alt="temp"/>
              <span className='iu-imagePreviewRemove' onClick={() => this.removeImage(index)}><span className="fa fa-minus"/></span>
            </div>
          )}
        </div>
      </div>
    );
  }
}