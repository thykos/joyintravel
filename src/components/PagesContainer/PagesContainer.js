import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

// function scrollTo(element, to, duration) {
//   const start = element.scrollTop;
//   const change = to - start;
//   const increment = 20;
//   let currentTime = 0;
//
//   function animateScroll() {
//     currentTime += increment;
//     element.scrollTop = Math.easeInOutQuad(currentTime, start, change, duration);
//     if (currentTime < duration) {
//       setTimeout(animateScroll, increment);
//     }
//   }
//   animateScroll();
// }
//
// Math.easeInOutQuad = function easeInOutQuad(ct, start, change, duration) {
//   let currentTime = ct / (duration * 0.5);
//   if (currentTime < 1) return currentTime / 2 * currentTime * currentTime + start;
//   currentTime--;
//   return -change / 2 * (currentTime * (currentTime - 2) - 1) + start;
// };

export default class PagesContainer extends Component {
  static propTypes = {
    components: PropTypes.array.isRequired
  };

  // componentDidMount() {
  //   window.addEventListener('mousewheel', this.onScroll);
  //   document.addEventListener('keyup', this.onArrowPress);
  // }
  //
  // componentWillUnmount() {
  //   window.removeEventListener('mousewheel', this.onScroll);
  //   document.removeEventListener('keyup', this.onArrowPress);
  // }
  //
  // onScroll = (event) => {
  //   const currentPosition = document.body.scrollTop;
  //   const wh = window.innerHeight;
  //   if (currentPosition === wh) return;
  //   const direction = event.wheelDelta >= 0 ? -1 : 1;
  //   const newPosition = currentPosition + (wh * direction);
  //   scrollTo(document.body, newPosition, 600);
  // };
  //
  // onArrowPress = (event) => {
  //   const currentPosition = document.body.scrollTop;
  //   const wh = window.innerHeight;
  //   if (currentPosition === wh) return;
  //   const direction = event.key === 'ArrowDown' ? 1 : -1;
  //   const newPosition = currentPosition + (wh * direction);
  //   scrollTo(document.body, newPosition, 600);
  // };

  render() {
    const { components } = this.props;
    return (
      <div className='pagesContainerContainer'>
        {components.map((component, idx) =>
          <div key={idx} className='pagesContainerPage'>
            {component.component
              ? React.createElement(component.component, component.props)
              : component}
          </div>
        )}
      </div>
    );
  }
}
