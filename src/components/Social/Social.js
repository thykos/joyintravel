import React, { Component } from 'react';
import './styles.scss';

const socials = [
  {icon: 'fa fa-facebook-f', link: 'https://www.facebook.com/joyintravelua'},
  {icon: 'fa fa-vk', link: 'https://vk.com/joyintravel'},
  {icon: 'fa fa-instagram', link: 'https://www.instagram.com/joyintravel'}
];

export default class Social extends Component {
  render() {
    return (
      <div className='socialContainer'>
        <div className='socialList'>
          {socials.map((social, idx) =>
            <a key={idx}
               className='socialLink'
               href={social.link}
               target="_blank"><i className={social.icon}/></a>
          )}
        </div>
      </div>
    );
  }
}
