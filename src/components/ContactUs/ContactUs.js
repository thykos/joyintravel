import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';
import { Button } from 'react-bootstrap';
import './styles.scss';
import { reduxForm, Field, reset } from 'redux-form';
import errorsHandler from '../../helpers/errorsHandler';
import { toastr } from 'react-redux-toastr';
import { connect } from 'react-redux';
import { renderInput, renderTextArea, renderMaskedInput } from '../../helpers/formHelpers';

class ContactUs extends Component {
  static propTypes = {
    color: PropTypes.string,
    reset: PropTypes.func.isRequired
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };

  static Form = reduxForm({
    form: 'contactUsForm',
    fields: ['name', 'email', 'phone', 'message']
  })(({handleSubmit}) =>
    <form onSubmit={handleSubmit} className="contactUsForm">
      <div className="contactUsInputs">
        <Field component={renderInput}
               placeholder="Имя"
               required
               className="contactUsInput"
               name="name"
               />
        <Field component={renderMaskedInput}
               placeholder="+38(095)-123-45-67"
               name="phone"
               required
               className="contactUsInput"
               mask="+38(011)-111-11-11"
               />
        <Field component={renderInput}
               placeholder="Email"
               type="email"
               required
               className="contactUsInput"
               name="email"
               />
      </div>

      <Field component={renderTextArea}
             placeholder="Сообщение"
             required
             className="contactUsInput contactUsTextArea"
             name="message"
      />

      <div className="contactUsBtnWrapper">
        <Button bsSize="large" type="submit" className="contactUsBtn">Отправить</Button>
      </div>
    </form>
  );

  onSubmit = (formData) => {
    return this.context.client.post('/messages', {data: {resource: formData}})
      .then(() => {
        this.props.reset('contactUsForm');
        toastr.success('Ваше сообщение отправлено');
      })
      .catch(response => {
        const keys = {
          name: 'Имя',
          phone: 'Телефон',
          email: 'Email',
          message: 'Сообщение'
        };
        const errors = errorsHandler(response);
        const errorMessage = Object.keys(errors)
          .map(key => `${keys[key]}: ${errors[key]}`).join(';\n');
        toastr.error(errorMessage);
      });
  };

  render() {
    const { color } = this.props;
    return(
      <Card color={color} className="contactUsAngle">
        <div className="contactUsWrapper">
          <div className="contactUsHeader">Остались вопросы - напиши нам!</div>
          <div className="contactUsBody">
            <ContactUs.Form onSubmit={this.onSubmit}/>
          </div>
        </div>
      </Card>
    )
  }
}

export default connect(null, { reset })(ContactUs);