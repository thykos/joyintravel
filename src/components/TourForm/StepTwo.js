import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { Button } from 'react-bootstrap';
import moment from 'moment';
import { times, get } from 'lodash';
import { renderTextArea, renderImageUploader, renderHiddenInput, renderTagsInput } from '../../helpers/formHelpers';

class StepTwo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [{}]
    };
  };

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    tour: PropTypes.object.isRequired
  };


  addImage = () => {
    const { images } = this.state;
    if (images.length > 4) return;
    this.setState({
      images: [...this.state.images, {}]
    });
  };

  render() {
    const { handleSubmit, anyTouched, tour: { start_date, end_date } } = this.props;
    console.log(this.props.tour);
    const { images } = this.state;
    const daysCount = start_date && moment(end_date).add(1, 'day').startOf('day').diff(moment(start_date).startOf('day'), 'days');
    const days = daysCount && times(daysCount, day =>
        ({
          day: day + 1,
          date: moment(start_date).add(day, 'day').toDate()
        })
      );
    return(
      <div className="well">
        <form onSubmit={handleSubmit} className='tf-form'>
          <div className="row">
            <div className="col-xs-6">
              <div className="form-group">
                <label>Что включено</label>
                <Field component={renderTagsInput}
                       name="included.included"/>
              </div>
            </div>
            <div className="col-xs-6">
              <div className="form-group">
                <label>Почему стоит поехать</label>
                <Field component={renderTagsInput} name="included.why"/>
              </div>
            </div>
            <div className="col-xs-6">
              <div className="form-group">
                <label>Что взять с собой</label>
                <Field component={renderTagsInput} name="included.get"/>
              </div>
            </div>
          </div>

          <h3>Описание дней</h3>

          <div className="row">
            { get(days, 'length') && days.map((item, idx) =>
              <div key={idx} className="col-xs-4">
                <h4 className="text-center">{`День ${item.day}`}</h4>
                <h5 className="text-center">{`(${moment(item.date).format('DD-MM-YYYY')})`}</h5>
                <div className="form-group">
                  <label>Фото</label>
                  <Field component={renderImageUploader} name={`days.${idx}.image`}/>
                </div>
                <div className="form-group">
                  <label>Описание</label>
                  <Field component={renderTextArea}
                         name={`days.${idx}.description`}
                         placeholder="Input description"/>
                </div>
                <Field name={`days.${idx}.date`} component={field => renderHiddenInput({...field, input: {...field.input, value: item.date}})}/>
                <Field name={`days.${idx}.day`} component={field => renderHiddenInput({...field, input: {...field.input, value: item.day}})}/>
              </div>)}
          </div>

          <h3>Фотографии <Button className="pull-right" disabled={images.length > 4} onClick={this.addImage} bsSize="small">+</Button></h3>

          <div className="row">
            { images.map((item, idx) =>
              <div key={idx} className="col-xs-4">
                <h3 className="text-center">{`Фото ${idx + 1}`}</h3>
                <div className="form-group">
                  <label>Фото</label>
                  <Field component={renderImageUploader} name={`images.${idx}.image`}/>
                  <div className="form-group">
                    <label>Описание</label>
                    <Field component={renderTextArea}
                           name={`images.${idx}.description`}
                           placeholder="Input description"/>
                  </div>
                </div>
              </div>)}
          </div>


          <div className="text-center">
            <Button type="submit" disabled={!anyTouched} className="btn btn-primary btn-lg">Сохранить</Button>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'tourFormStepTwo',
  fields: ['images', 'included', 'days']
})(StepTwo);
