import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { times } from 'lodash';
import moment from 'moment';
import StepOne from './StepOne';
import StepTwo from './StepTwo';
import errorsHandler from '../../helpers/errorsHandler';
import './styles.scss';
import { SubmissionError } from 'redux-form';
import { toastr } from 'react-redux-toastr';

export default class TourForm extends Component {
  static propTypes = {
    tour: PropTypes.object,
    step: PropTypes.string,
    onSubmit: PropTypes.func
  };

  static defaultProps = {
    tour: {}
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };

  createTour = formData => {
    return this.context.client.post('/tours', { data: { resource: formData } })
      .then((response) => {
        toastr.success('Тур успешно создан');
        this.props.onSubmit(`/admin/tours/${response.resource.slug}?step=2`);
      })
      .catch(response => {
        throw new SubmissionError(errorsHandler(response));
      });
  };

  updateTour = formData => {
    const { tour, onSubmit } = this.props;
    const daysCount = moment(tour.end_date).add(1, 'day').startOf('day').diff(moment(tour.start_date).startOf('day'), 'days');
    const days = daysCount && times(daysCount, day =>
        ({
          day: day + 1,
          date: moment(tour.start_date).add(day, 'day').toDate()
        })
      );
    const data = {
      ...formData,
      days: formData.days.map((item, idx) => ({
        ...item,
        ...days[idx]
      }))
    };
    return this.context.client.put(`/tours/${tour.slug}`, { data: { resource: data } })
      .then(() => {
        toastr.success('Тур успешно обновлен');
        onSubmit('/admin/tours');
      })
      .catch(response => {
        throw new SubmissionError(errorsHandler(response));
      });
  };

  render() {
    const { step, tour } = this.props;
    return (
      <div className="row">
        <div className="col-lg-offset-2 col-lg-8 col-md-12 col-sm-12 col-xs-12">
          { parseFloat(step) !== 2 && <StepOne onSubmit={this.createTour} label={tour.id ? 'Сохранить' : 'Создать'}/> }
          { parseFloat(step) === 2 && <StepTwo tour={tour} onSubmit={this.updateTour}/> }
        </div>
      </div>
    );
  }
}
