import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { Button } from 'react-bootstrap';
import { renderInput, renderTextArea, renderSelect, renderImageUploader } from '../../helpers/formHelpers';

const currencies = [
  {value: '$'},
  {value: '€'},
  {value: 'грн'}
];

const colors = [
  { value: '#FEE900', label: 'sun'},
  { value: '#fea203', label: 'orange'},
  { value: '#ff0063', label: 'playboy'},
  { value: '#03cbff', label: 'marine'},
  { value: '#4dce00', label: 'forest'},
  { value: '#003d92', label: 'sea'},
  { value: '#d80183', label: 'pink'}
];

class StepOne extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired
  };

  render() {
    const { handleSubmit } = this.props;
    return(
      <div className="well">
        <form onSubmit={handleSubmit} className='tf-form'>
          <div className="row">
            <div className="col-xs-6">
              <div className="form-group">
                <label>Название</label>
                <Field type="text"
                       component={renderInput}
                       name="title"
                       placeholder="Input title"/>
              </div>
            </div>
            <div className="col-xs-3">
              <div className="form-group">
                <label>Валюта</label>
                <Field name="currency" component={field => renderSelect(field, currencies)}/>
              </div>
            </div>
            <div className="col-xs-3">
              <div className="form-group">
                <label>Цена</label>
                <Field type="number"
                       component={renderInput}
                       name="price"
                       placeholder="100"/>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-xs-4">
              <div className="form-group">
                <label>Цвет</label>
                <Field name="color" component={field => renderSelect(field, colors)}/>
              </div>
            </div>
            <div className="col-xs-4">
              <div className="form-group">
                <label>Начало тура</label>
                <Field type="date"
                       component={renderInput}
                       name="start_date"/>
              </div>
            </div>
            <div className="col-xs-4">
              <div className="form-group">
                <label>Конец тура</label>
                <Field type="date"
                       component={renderInput}
                       name="end_date"/>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-xs-6">
              <div className="form-group">
                <label>Обложка</label>
                <Field component={renderImageUploader} name="cover"/>
              </div>
            </div>
            <div className="col-xs-6">
              <div className="form-group">
                <label>Заставка</label>
                <Field component={renderImageUploader} name="background"/>
              </div>
            </div>
          </div>



          <div className="row">
            <div className="col-xs-12">
              <div className="form-group">
                <label>Описание</label>
                <Field component={renderTextArea}
                       name="description"
                       placeholder="Input description"/>
              </div>
            </div>
          </div>


          <div className="text-center">
            <Button type="submit" className="btn btn-primary btn-lg">Создать</Button>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({
  form: 'tourFormStepOne',
  fields: ['title', 'cover', 'background', 'currency', 'color',
    'description', 'start_date', 'end_date', 'price']
})(StepOne);