import React, { Component } from 'react';
import './styles.scss';
import Card  from '../Card/Card';
import aboutImg from '../../static/images/aboutImg.png';

export default class About extends Component {
  render() {
    return (
    <Card>
      <div className='aboutWrapper'>
        <div className='aboutImgWrapper'>
          <img src={aboutImg} alt="About Joy in Travel"/>
        </div>
        <div className='aboutText'>
          <div className='aboutHeader'>О нас</div>
          <div className='aboutTitle'>
            <div>Что такое</div>
            <div className='aboutBrand'>Joy in Travel?</div>
          </div>
          <div className='aboutContent'>
            Ассоциация молодежного туризма, которая создана активными студентами в 2014 году.
            Наша цель - развитие внутреннего туризма, организация доступного и качественного отдыха для студентов.
            Для нас это больше, чем просто проект.
            Это возможность показать молодеже нашу Страну!
            Мы путешествуем Украиной почти 3 года!
            За этот период з нами отдохнули уже больше 1500 студентов.
          </div>
        </div>
      </div>
    </Card>
    );
  }
}
