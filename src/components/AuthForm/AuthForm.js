import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { Button } from 'react-bootstrap';
import './styles.scss';
import { setUser } from '../../reducers/auth';
import { connect } from 'react-redux';
import { toastr } from 'react-redux-toastr';

class AuthForm extends Component {
  static propTypes = {
    setUser: PropTypes.func.isRequired
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };

  static Form = reduxForm({
    form: 'authForm',
    fields: ['email', 'password']
  })(
    ({ handleSubmit, fields}) => {
      return (
        <div className='authForm-form'>
          <form onSubmit={handleSubmit}>
            <Field component="input" className="form-control" type="email" name="email" placeholder="email" {...fields.email}/>
            <Field component="input" className="form-control" type="password" name="password" placeholder="password" {...fields.password}/>
            <div className='authForm-btnWrapper'>
              <Button type="submit" bsStyle="primary">Sign In</Button>
            </div>
          </form>
        </div>
      );
    }
  );

  handleSubmit = (formData) => {
    this.context.client.post('/auth/sign_in', { data: formData })
      .then(response => {
        this.props.setUser(response.data);
      })
      .catch(response => toastr.error(response.errors.join(';\n')));
  };

  render() {
    return (
      <div className='authForm-wrapper'>
        <AuthForm.Form onSubmit={(formData) => this.handleSubmit(formData)}/>
      </div>
    );
  }
}

export default connect(null, { setUser })(AuthForm);