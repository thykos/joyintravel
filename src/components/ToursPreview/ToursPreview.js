import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import 'moment/locale/ru';
import { Link } from 'react-router-dom';
import './styles.scss';

moment.locale('ru');

export default class ToursPreview extends Component {
  static propTypes = {
    tours: PropTypes.array.isRequired
  };

  render() {
    const { tours } = this.props;
    return (
      <div className='toursPreview-container'>
        <div className='toursPreview-wrapper'>
          <div className='toursPreview-body'>
            <div className='toursPreview-title'>Актуальные поездки</div>
            <div className='toursPreview-gallery'>
              {tours.length > 0 && tours.map((tour, idx) =>
                <div className='toursPreview-tour' key={idx} style={{backgroundImage: `url(${tour.background.thumb.url})`}}>
                  <Link to={`/tours/${tour.slug}`}>
                    <div className='toursPreview-content'>
                      <div className='toursPreview-date'>
                        <div><strong>{moment(tour.start_date).format('MMMM')}</strong></div>
                        <div>{moment(tour.start_date).format('D')}</div>
                      </div>
                      <div className='toursPreview-titleWrapper'>
                        <div>{tour.price}{tour.currency}</div>
                        <div className='toursPreview-name'><strong>{tour.title}</strong></div>
                      </div>
                    </div>
                  </Link>
                </div>
              )}
              {tours.length < 1 && <div className='toursPreview-placeholder'>На данный момент нет актуальных туров</div>}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

