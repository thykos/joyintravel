import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '../Card/Card';
import './styles.scss';
import classnames from 'classnames';

const colors = {
  '#FEE900': 'included-sun',
  '#fea203': 'included-orange',
  '#ff0063': 'included-playboy',
  '#03cbff': 'included-marine',
  '#4dce00': 'included-forest',
  '#003d92': 'included-sea',
  '#d80183': 'included-pink'
};

const titles = {
  included: 'Что включено',
  get: 'Что взять с собой',
  why: 'Почему стоит к нам присоедениться?'
};

const icons = {
  included: 'fa fa-plus',
  get: 'fa fa-suitcase',
  why: 'fa fa-info'
};

export default class Included extends Component {
  static propTypes = {
    included: PropTypes.object.isRequired,
    color: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      tab: 'included'
    };
  }

  onClick = (tab) => {
    this.setState({tab: tab});
  };

  render() {
    const { included, color } = this.props;
    const { tab } = this.state;
    const tabs = ['included', 'get', 'why'];
    return (
      <Card color={color} className='included-angle'>
        <div className={classnames('included-wrapper', colors[color])}>
          <div className='included-tabs'>
            <ul>
              {tabs.map((item, idx) =>
                <li key={idx}
                    className={tab === item ? 'included-active' : ''}
                    onClick={() => this.onClick(item)}>
                  <i className={classnames(icons[item], 'included-icon')}/> {titles[item]}
                </li>
              )}
            </ul>
          </div>
          <div className='included-content'>
            <div className='included-title'>{titles[tab]}</div>
            <ul className='included-list'>
              {included[tab].map((item, idx) =>
                <li key={idx}>{idx + 1}. {item}</li>
              )}
            </ul>
          </div>
        </div>
      </Card>
    );
  }
}
