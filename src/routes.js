import React from 'react';
import Home from './containers/Home/Home';
import TourShow from './containers/TourShow/TourShow';
import AdminToursIndex from './containers/Admin/Pages/Tours/Index';
import AdminCreateTour from './containers/Admin/Pages/Tours/Create';
import AdminEditTour from './containers/Admin/Pages/Tours/Edit';
import TouristsIndex from './containers/Admin/Pages/Tourists/Index';
import { Route, Switch } from 'react-router-dom';

export default () => (
  <Switch>
    <Route exact path='/' component={Home}/>
    <Route path='/tours/:slug' component={TourShow}/>
  </Switch>
);


export const AdminRoutes = () => (
  <Switch>
    <Route exact path='/admin/tours' component={AdminToursIndex}/>
    <Route exact path='/admin/tours/new' component={AdminCreateTour}/>
    <Route exact path='/admin/tours/:slug' component={AdminEditTour}/>
    <Route exact path='/admin/tourists' component={TouristsIndex}/>
  </Switch>
);

