const SET_TOURS = 'tours/SET_TOURS';
const SET_TOUR = 'tours/SET_TOUR';
const CLEAR_TOUR = 'tours/CLEAR_TOUR';
const initialState = {
  list: {
    meta: {},
    resources: []
  },
  one: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_TOURS:
      return {
        ...state,
        list: action.payload
      };
    case SET_TOUR:
      return {
        ...state,
        one: action.payload
      };
    case CLEAR_TOUR:
      return {
        ...state,
        one: {}
      };
    default:
      return state;
  }
}

export function setTours(resources) {
  return {
    type: SET_TOURS,
    payload: resources
  };
}

export function setTour(resource) {
  return {
    type: SET_TOUR,
    payload: resource
  };
}

export function clearTour() {
  return {
    type: CLEAR_TOUR
  };
}
