import { createStore, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import {reducer as toastrReducer} from 'react-redux-toastr'
import background from './background';
import tours from './tours';
import spinner from './spinner';
import auth from './auth';
import tourists from './tourists';

export default createStore(
  combineReducers({
    background,
    tours,
    spinner,
    tourists,
    auth,
    form: formReducer,
    toastr: toastrReducer
  }),
  process.env.NODE_ENV === 'development'
  && window.__REDUX_DEVTOOLS_EXTENSION__
  && window.__REDUX_DEVTOOLS_EXTENSION__(),
);
