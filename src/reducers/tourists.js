const SET_TOURISTS = 'tours/SET_TOURISTS';
const initialState = {
  list: {
    meta: {},
    resources: []
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_TOURISTS:
      return {
        ...state,
        list: action.payload
      };
    default:
      return state;
  }
}

export function setTourists(response) {
  return {
    type: SET_TOURISTS,
    payload: response
  };
}
