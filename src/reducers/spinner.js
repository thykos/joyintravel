const SHOW_SPINNER = 'spinner/SHOW_SPINNER';
const HIDE_SPINNER = 'spinner/HIDE_SPINNER';

export default function(state = { show: false }, action) {
  switch (action.type) {
    case SHOW_SPINNER:
      return {
        show: true
      };
    case HIDE_SPINNER:
      return {
        show: false
      };
    default:
      return state;
  }
}

export function showSpinner() {
  return { type: SHOW_SPINNER };
}

export function hideSpinner() {
  return { type: HIDE_SPINNER };
}
