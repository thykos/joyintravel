import initialState from '../static/images/backgrounds/bg.jpg';
const SET_BACKGROUND = 'background/SET_BACKGROUND';
const RESET_BACKGROUND = 'background/RESET_BACKGROUND';

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_BACKGROUND:
      return action.payload;
    case RESET_BACKGROUND:
      return initialState;
    default:
      return state;
  }
}

export function setBackground(src) {
  return {
    type: SET_BACKGROUND,
    payload: src
  };
}

export function resetBackground() {
  return {
    type: RESET_BACKGROUND
  };
}
