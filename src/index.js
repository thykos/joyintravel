import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import client from './helpers/ApiClient';
import Root from './containers/Root/Root';
import { BrowserRouter, Route } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import store from './reducers/store';

client.setStore(store);

ReactDOM.render(
  <Provider store={store}>
      <BrowserRouter>
          <Route component={Root}/>
      </BrowserRouter>
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();
