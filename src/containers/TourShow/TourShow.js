import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { get, isEqual } from 'lodash';
import PagesContainer from '../../components/PagesContainer/PagesContainer';
import Hero from '../../components/Hero/Hero';
import Day from '../../components/Day/Day';
import Included from '../../components/Included/Included';
import ContactUs from '../../components/ContactUs/ContactUs';
import Subscribe from '../../components/Subscribe/Subscribe';
import { setBackground, resetBackground } from '../../reducers/background';
import { setTour, clearTour } from '../../reducers/tours';
import moment from 'moment';

// moment.locale('ru');
class TourShow extends Component {
  static propTypes = {
    tour: PropTypes.object,
    setTour: PropTypes.func.isRequired,
    clearTour: PropTypes.func.isRequired,
    resetBackground: PropTypes.func.isRequired,
    setBackground: PropTypes.func.isRequired
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };

  componentWillReceiveProps(nextProps) {
    if (!isEqual(nextProps.tour, this.props.tour)) {
      this.props.setBackground(`${nextProps.tour.background.large.url}`);
    }
  }

  componentDidMount() {
    this.context.client.get(`/tours/${this.props.match.params.slug}`)
      .then(response => this.props.setTour(response.resource));
  }

  componentWillUnmount() {
    this.props.resetBackground();
    this.props.clearTour();
  }

  render() {
    const { tour } = this.props;
    const color = get(tour, 'color');
    const components = tour.id ? [
      {
        component: Hero,
        props: {
          color,
          mainFields: [<strong>{moment(tour.start_date).format('D')}</strong>, moment(tour.start_date).format('MMMM')],
          fields: [tour.title.split(' '), `${tour.price} ${tour.currency}`]
        }
      },
      ...tour.days.map((day, idx) => ({
        component: Day,
        props: {
          color,
          day: day,
          key: idx
        }
      })),
      {
        component: Included,
        props: {
          color,
          included: tour.included
        }
      },
      {
        component: Subscribe,
        props: {
          color,
          tourId: tour.id
        }
      },
      {
        component: ContactUs,
        props: {
          color
        }
      }
    ] : [];
    return (
      <div>
        { tour && <PagesContainer components={components}/> }
      </div>
    );
  }
}

export default connect(state => ({
  tour: state.tours.one
}), { setBackground, setTour, resetBackground, clearTour })(TourShow);
