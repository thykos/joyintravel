import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import moment from 'moment';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { setTours } from '../../../../reducers/tours';
import './styles.scss';

class AdminToursIndex extends Component {
  static propTypes = {
    tours: PropTypes.object.isRequired,
    setTours: PropTypes.func.isRequired
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      tours: props.tours
    };
  }

  componentDidMount() {
    this.getTours();
  }

  getTours = () => {
    this.context.client.get('/tours')
      .then(response => this.props.setTours(response));
  };

  onRemove(tour) {
    this.context.client.del(`/tours/${tour.slug}`)
      .then(() => this.getTours());
  }

  render() {
    const { tours } = this.props;
    return (
      <div className='adminToursIndex-index'>
        <div className='adminToursIndex-header'>
          <h3>Список туров</h3>
          <div>
            <Link to="/admin/tours/new">
              <Button bsStyle="info">Создать</Button>
            </Link>
          </div>
        </div>
        {tours.resources.length > 0 &&
          <table className={classnames('table table-hover', 'adminToursIndex-table')}>
            <thead>
              <tr>
                <td>Заголовок</td>
                <td>Начало</td>
                <td>Конец</td>
                <td>Подробнее</td>
                <td>Удалить</td>
              </tr>
            </thead>
            <tbody>
            {tours.resources.map((tour) =>
              <tr key={tour.id}>
                <td>{tour.title}</td>
                <td>{moment(tour.start_date).format('DD-MMM-YYYY')}</td>
                <td>{moment(tour.end_date).format('DD-MMM-YYYY')}</td>
                <td>
                  <Link to={`/admin/tours/${tour.slug}`}>
                    <Button bsSize="xs" bsStyle="primary">Редактировать</Button>
                  </Link>
                </td>
                <td><Button bsSize="xs" onClick={() => this.onRemove(tour)} bsStyle="primary">&times;</Button></td>
              </tr>
            )}
            </tbody>
          </table>
        }
        {tours.resources.length < 1 && <h1 className="text-center">На данный момент нет туров</h1>}
      </div>
    );
  }
}

export default connect(state => ({ tours: state.tours.list }), { setTours })(AdminToursIndex);
