import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TourForm from '../../../../components/TourForm/TourForm';
import { connect } from 'react-redux';
import { setTour } from '../../../../reducers/tours';
import { parseQuery } from '../../../../helpers/queryHelpers';

class AdminEditTour extends Component {
  static propTypes = {
    tour: PropTypes.object.isRequired,
    setTour: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };


  componentDidMount() {
    this.context.client.get(`/tours/${this.props.match.params.slug}`)
      .then(response => this.props.setTour(response.resource));
  }

  onSubmit = (route) => {
    this.props.history.push(route);
  };

  render() {
    const { tour, location: { search } } = this.props;
    return (
      <div>
        <h2 className="text-center">Редактировать тур</h2>
        <TourForm step={parseQuery(search).step} tour={tour} onSubmit={this.onSubmit}/>
      </div>
    );
  }
}

export default connect(state => ({
  tour: state.tours.one
}), { setTour })(AdminEditTour);