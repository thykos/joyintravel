import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TourForm from '../../../../components/TourForm/TourForm';

export default class AdminCreateTour extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired
  };

  onSubmit = (route) => {
    this.props.history.push(route);
  };

  render() {
    return (
      <div>
        <h2 className="text-center">Создать тур</h2>
        <TourForm onSubmit={this.onSubmit}/>
      </div>
    );
  }
}
