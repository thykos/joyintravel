import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { setTourists } from '../../../../reducers/tourists';

class TouristsIndex extends Component {
  static propTypes = {
    tourists: PropTypes.object.isRequired,
    setTourists: PropTypes.func.isRequired
  };

  static defaultProps = {
    tourists: {}
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };

  componentDidMount() {
    this.context.client.get('/tourists')
      .then(response => this.props.setTourists(response));
  }

  render() {
    const { tourists } = this.props;
    return(
      <div>
        <h3>Список туров</h3>
        {tourists.resources ?
          <table className="table table-hover">
            <thead>
            <tr>
              <td className="text-center">Имя</td>
              <td className="text-center">Фамилия</td>
              <td className="text-center">Номер</td>
              <td className="text-center">Почта</td>
              <td className="text-center">ВУЗ</td>
              <td className="text-center">Студенческий</td>
              <td className="text-center">Соц сеть</td>
              <td className="text-center">Тур</td>
            </tr>
            </thead>
            <tbody>
            {tourists.resources.map((item, idx) =>
              <tr key={idx}>
                <td>{item.first_name}</td>
                <td>{item.last_name}</td>
                <td>{item.phone}</td>
                <td>{item.email}</td>
                <td className="text-center">{item.university || '-'}</td>
                <td className="text-center">{item.student_card || '-'}</td>
                <td className="text-center">
                  {item.social ?
                    <a href={item.social} target="_blank">
                      <Button bsSize="xs" bsStyle="info">
                        Перейти
                      </Button>
                    </a>
                    : <div>-</div>
                  }
                </td>
                <td className="text-center">
                  <Link to={`/tours/${item.tour_id}`}>
                    <Button bsSize="xs" bsStyle="primary">Перейти</Button>
                  </Link>
                </td>
              </tr>
            )}
            </tbody>
          </table>
        : <h1 className="text-center">На данный момент нет туристов</h1>
        }
      </div>
    );
  }
}

export default connect(state => ({
  tourists: state.tourists.list
}), { setTourists })(TouristsIndex);
