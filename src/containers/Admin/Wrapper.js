import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import AuthForm from '../../components/AuthForm/AuthForm';
import { get } from 'lodash';
import './styles.scss';
import { Link } from 'react-router-dom';
import { setUser, logout } from '../../reducers/auth';
import { AdminRoutes } from '../../routes';
import { toastr } from 'react-redux-toastr';

const links = [
  {to: "/admin/tours", label: 'Туры'},
  {to: "/admin/albums", label: 'Альбомы'},
  {to: "/admin/tourists", label: 'Туристы'}
];

class AdminWrapper extends Component {
  static propTypes = {
    user: PropTypes.object,
    location: PropTypes.object.isRequired,
    setUser: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };

  componentDidMount() {
    this.context.client.get('/auth/validate_token')
      .then(response => {
        this.props.setUser(response.data);
      })
      .catch(response => toastr.error(response.errors.join(';\n')));
  }

  render() {
    const { user, location: { pathname } } = this.props;
    const userExist = get(user, 'id');
    return (
      <div className='container'>
        {userExist &&
          <div className="adminWrapper">
            <nav className="navbar navbar-default">
              <div className="container">
                <div className="navbar-header">
                  <Link className="navbar-brand" to="/admin"><span>JT Admin</span></Link>
                </div>

                <ul className="nav navbar-nav">
                  {links.map((link, idx) =>
                    <li key={idx} className={classnames(pathname.includes(link.to) && 'active')}>
                      <Link to={link.to}>{link.label}</Link>
                    </li>
                  )}
                </ul>
                <ul className="nav navbar-nav navbar-right">
                  <li><Link to="/admin" onClick={this.props.logout}>Выйти</Link></li>
                </ul>
              </div>
            </nav>
            <div>
              <AdminRoutes/>
            </div>
          </div>
        }
        {!userExist && <AuthForm/>}
      </div>
    );
  }
}

export default connect(state => ({user: state.auth.user}), { setUser, logout })(AdminWrapper);
