import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './App.scss';
import Menu from '../../components/Menu/Menu';
import Routes from '../../routes';
import Social from '../../components/Social/Social';
import { connect } from 'react-redux';

class App extends Component {
  static propTypes = {
    spinner: PropTypes.object.isRequired,
    background: PropTypes.string.isRequired
  };

  render() {
    const { background, spinner, history: { push } } = this.props;
    return (
      <div className="appWrapper" style={{backgroundImage: `url(${background})`}}>
        <div className="app">
          <Menu push={push}/>
          <Social/>
          <div className='appContent'>
            <div className='childWrapper'>
              {spinner.show &&
                <div className="appSpinner">
                  <h1>LOADING...</h1>
                </div>
              }
              <Routes/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(state => ({
  background: state.background,
  spinner: state.spinner
}))(App);
