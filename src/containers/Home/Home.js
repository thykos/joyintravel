import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Hero from '../../components/Hero/Hero';
import ToursPreview from '../../components/ToursPreview/ToursPreview';
import About from '../../components/About/About';
import PagesContainer from '../../components/PagesContainer/PagesContainer';
import ContactUs from '../../components/ContactUs/ContactUs';
import { setTours } from '../../reducers/tours';
import { connect } from 'react-redux';

class Home extends Component {
  static propTypes = {
    tours: PropTypes.object.isRequired,
    setTours: PropTypes.func.isRequired
  };

  static contextTypes = {
    client: PropTypes.object.isRequired
  };

  componentDidMount() {
    this.context.client.get('/tours')
      .then(response => this.props.setTours(response));
  }

  render() {
    const { tours } = this.props;
    const components = [
      {
        component: Hero,
        props: {
          title: 'Joy in travel',
          mainFields: ['От студентов', 'для студентов'],
          fields: ['ассоциация', 'молодежного', 'туризма']
        }
      },
      {
        component: About
      },
      {
        component: ToursPreview,
        props: {
          tours: tours.resources
        }
      },
      {
        component: ContactUs,
      }
    ];
    return (
      <div>
        <PagesContainer components={components}/>
      </div>
    );
  }
}

export default connect(state => ({ tours: state.tours.list }), { setTours })(Home);
