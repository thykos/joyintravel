import React, { Component } from 'react';
import PropTypes from 'prop-types';
import client from '../../helpers/ApiClient';
import App from '../App/App';
import AdminWrapper from '../Admin/Wrapper';
import { Route } from 'react-router-dom';
import ReduxToastr from 'react-redux-toastr'

export default class Root extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired
  };

  static childContextTypes = {
    client: PropTypes.object.isRequired
  };

  getChildContext() {
    return { client: client };
  }

  render() {
    const { location: { pathname } } = this.props;
    return (
      <div>
        {pathname.includes('admin')
          ? <Route path="/admin" component={AdminWrapper}/>
          : <Route component={App}/>
        }
        <ReduxToastr
          timeOut={4000}
          newestOnTop={false}
          preventDuplicates
          position="top-right"
          transitionIn="fadeIn"
          transitionOut="fadeOut"
          progressBar/>
      </div>
    );
  }
}
