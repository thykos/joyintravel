import { isObject, includes, isArray } from 'lodash';

export default function errorsHandler(response) {
  if (response) {
    if (response.errors) {
      const result = {};
      if (isObject(response.errors) && !isArray(response.errors)) {
        const keys = Object.keys(response.errors);
        keys.forEach((key) => {
          if (includes(key, '.')) {
            const parsedKey = key.split('.');
            const origin = parsedKey[0];
            const property = parsedKey[1];
            if (!result[origin]) result[origin] = {};
            result[origin][property] = response.errors[key].join(';');
          } else {
            result[key] = response.errors[key].join(';');
          }
        });
        return result;
      }
    }
  }
  return false;
}
