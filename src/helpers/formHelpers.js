import React from 'react';
import ImageUploader from '../components/ImageUploader/ImageUploader';
import TagsInput from '../components/TagsInput/TagsInput';
import MaskedInput from 'react-maskedinput';
import classnames from 'classnames';
import { uniq } from 'lodash';

export const renderInput = field => {
  return(
    <span>
      <input type={field.type}
             {...field.input}
             required={field.required}
             placeholder={field.placeholder}
             className={classnames(field.className, 'form-control')}/>
      {field.meta.touched &&
        field.meta.error &&
        <span className="f-error">{field.meta.error}</span>
      }
    </span>
  );
};
export const renderHiddenInput = field => {
  return(
    <span>
      <input type='hidden'
             {...field.input}
             required={field.required}
             className="form-control"/>
      {field.meta.touched &&
        field.meta.error &&
        <span className="f-error">{field.meta.error}</span>
      }
    </span>
  );
};

export const renderTextArea = field => {
  return(
    <span>
      <textarea type={field.type}
                {...field.input}
                required={field.required}
                placeholder={field.placeholder}
                className={classnames(field.className, 'form-control')}/>
      {field.meta.touched &&
        field.meta.error &&
        <span className="f-error">{field.meta.error}</span>
      }
    </span>
  );
};

export const renderSelect = (field, options) => {
  return(
    <span>
      <select
        {...field.input}
        required={field.required}
        className={classnames(field.className, 'form-control')}>
          {options.map((option, idx) =>
            <option key={idx} value={option.value}>
              {option.label || option.value}
            </option>)
          }
        </select>
        {field.meta.touched &&
          field.meta.error &&
          <span className="f-error">{field.meta.error}</span>
        }
        </span>
  );
};

export const renderImageUploader = (field) => {
  return(
    <span>
      <ImageUploader {...field.input}
                     className={classnames(field.className, 'form-control')}
                     onTempImage={(image) => field.input.onChange(image)}/>
      {field.meta.touched &&
        field.meta.error &&
        <span className="f-error">{field.meta.error}</span>
      }
    </span>
  );
};
export const renderMaskedInput = (field) => {
  return(
    <span>
      <MaskedInput {...field.input}
                   required={field.required}
                   className={classnames(field.className, 'form-control')}
                   placeholder={field.placeholder}
                   value={field.input.value.length ? field.input.value : field.value}
                   mask={field.mask}/>
      {field.meta.touched &&
        field.meta.error &&
        <span className="f-error">{field.meta.error}</span>
      }
    </span>
  );
};

export const renderTagsInput = (field) => {
  const onChange = (value, input) => {
    const newValue = value.length < input.value.length ? value : uniq([...(input.value || []), ...value]);
    return input.onChange(newValue);
  };
  const value = field.input.value === '' ? [] : field.input.value;
  return(
    <span>
        <TagsInput {...field.input} value={value} onChange={(value) => onChange(value, field.input)}/>
        {field.meta.touched &&
          field.meta.error &&
          <span className="f-error">{field.meta.error}</span>
        }
        </span>
  );
};
