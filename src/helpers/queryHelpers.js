export const parseQuery = query => {
  const values = {};
  query.substring(1).split('&')
    .map(item => {
      const query = item.split('=');
      return values[[query[0]]] = query[1];
    });
  return values;
};
