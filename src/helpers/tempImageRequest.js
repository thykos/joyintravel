export default (client, file) =>
  client.post('/temp_images', {attach: {'resource[image]': file}})
    .then(response => {
      return response.resource;
    })
    .catch((response) => {throw response}); // eslint-disable-line no-throw-literal
